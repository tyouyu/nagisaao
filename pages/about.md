---
layout: page 
title: 关于
permalink: /about/
description: 2015年开始用Wordpress搭建博客，2018年迁移至Jekyll，源码保存在Github，部署于Netlify，评论系统由Valine支持，文章均为Juby的自说自话。
---

### 关于

------------------

#### 博客

2015年开始用Wordpress搭建博客，2018年迁移至Jekyll，源码保存在[GitLab](https://gitlab.com/jubyshu/nagisaao)，部署于Netlify，评论系统由Valine支持，文章均为Juby的自说自话。

-------------------

#### 自己

很喜欢痖弦写给妻子的诗中的一句：想着，生活着，偶尔也微笑着，既不快活也不不快活。我的人生就是这样的吧，没有什么可卖的，也并不想买什么，只愿侍弄好自己的菜园，在种菜的时候，也想栽下一丛蔷薇。

经常出没于自己的内心世界，以及[twitter](https://twitter.com/jubyshu)和[telegram](https://t.me/jubyshu)。如果你想支持我，可以使用[paypal](https://www.paypal.me/jubyshu)。

目前的兴趣：R，日语，棒球，跑步，科幻，······

喜欢的作家：鲁迅，芥川龙之介，博尔赫斯，夏目漱石，兰姆，陀思妥耶夫斯基。

喜欢的乐队：The Beatles，Eagles，Queen，KISS，GLAY，X Japan，JUDY AND MARY，Every Little Thing，Do As Infinity。

-------------------

#### 感谢

主题来源：[leopardpan](https://github.com/leopardpan/leopardpan.github.io/) & [onevcat](https://github.com/onevcat/vno-jekyll)  
评论通知：[Deserts](https://deserts.io/valine-admin-document/)  
功能扩展：[fooleap](https://blog.fooleap.org/)

-------------------

#### 订阅

以下是我自用的几个RSS订阅地址，由RSSHub生成。

- [aeon]( http://rss.rosemary.ink/aeon/home)：a world of ideas。
- [telling,](http://rss.rosemary.ink/telling/story)：あなただけに言うね。
- [联合文学](http://rss.rosemary.ink/unitas/recommend)：因为有点喜欢文学，所以生活变得不一样。
- [soar](http://rss.rosemary.ink/soar/home)：人の持つ可能性が広がる瞬間を捉え、伝えていくメディア。
- [BookBang](http://rss.rosemary.ink/bookbang/news)：新聞・出版社の書評まとめ読み！読書家のための本の総合情報サイト。
- [南沢奈央の読書日記](http://rss.rosemary.ink/bookbang/serial/minamisawanao)：ドラマや舞台、ラジオナビゲーターとして活躍する女優の南沢奈央さんは大の読書家。毎週、南沢さんが近況を交えつつ、興味のおもむくままに様々なジャンルの本を紹介します。